import 'package:flutter/material.dart';

const colorBlueDeeper= const Color(0xff001F4E);
const selectedColor = const Color(0xFF4ACBEA);
const colorPrimary=const Color(0xff008577);
const  colorPrimaryDark=const Color(0xff00574B);
const  colorAccent=const Color(0xffD81B60);
const  colorPurple=const Color(0xff8E2C9D);
const  colorGreyBlue=const Color(0xff2C3F5F);
const  colorBlack=const Color(0xff060405);
const  colorDeepBlue=const Color(0xff071834);
const  colorLemonGreen=const Color(0xff13AC06);
const  colorRedOrange=const Color(0xffED3338);
const  colorBlueLight=const Color(0xff0098D8);
const  colorRed=const Color(0xffEB0C42);
const  colorOrange=const Color(0xffFD8D07);
const  colorGreen=const Color(0xff0D9E01);
const  colorOrangeDeep=const Color(0xffFE0000);
const  colorAsh=const Color(0xffDBDBDB);
const  colorYellow=const Color(0xffFDC607);
const  colorBlueMild=const Color(0xff0D5E8B);
const  colorBlueBlack=const Color(0xff083B58);
const  viewBg=const Color(0xfff1f5f8);
const  album_title=const Color(0xff4c4c4c);
const  meatUpTheme=const Color(0xffF1B258);
const  meatUpTheme2 =const Color(0xffACBB16);
const  discountPink =const Color(0xffF0425C);
const  discountPink2 =const Color(0xffFCD9DE);





const  tileColor =const Color(0xAAA1A9AA);
const  backgroundColor =const Color(0xFFF5F5F5);
const  hintTextColor =const Color(0xFFE7E7E7);
const  activeTextColor =const Color(0xFF4A4D4E);



class Constants{

  static String appName = "Social app";

  //Colors for theme
  static Color lightPrimary = Color(0xfffcfcff);
  static Color darkPrimary = Colors.black;
  static Color lightAccent = Colors.blue;
  static Color darkAccent = Colors.blueAccent;
  static Color lightBG = Color(0xfffcfcff);
  static Color darkBG = Colors.black;
  static Color badgeColor = Colors.red;




  static ThemeData lightTheme = ThemeData(
    backgroundColor: lightBG,
    primaryColor: colorBlueDeeper,
    accentColor:  lightAccent,
    cursorColor: lightAccent,
    scaffoldBackgroundColor: lightBG,
    appBarTheme: AppBarTheme(
      elevation: 0,
      textTheme: TextTheme(
        title: TextStyle(
          color: darkBG,
          fontSize: 18.0,
          fontWeight: FontWeight.w800,
        ),
      ),
    ),
  );

  static ThemeData darkTheme = ThemeData(
    brightness: Brightness.dark,
    backgroundColor: darkBG,
    primaryColor: colorDeepBlue,
    accentColor: colorBlueMild,
    scaffoldBackgroundColor: darkBG,
    cursorColor: darkAccent,
    appBarTheme: AppBarTheme(
      elevation: 0,
      textTheme: TextTheme(
        title: TextStyle(
          color: lightBG,
          fontSize: 18.0,
          fontWeight: FontWeight.w800,
        ),
      ),
    ),
  );


}