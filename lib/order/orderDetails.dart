import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:farmcrowdyfoods/Engine/OrderDetailsPojo.dart';
import 'package:farmcrowdyfoods/Engine/httpDetails.dart';
import 'package:farmcrowdyfoods/colorconst.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:intl/intl.dart';

class OrderDetails extends StatefulWidget {
  final int id;
  final String status;
  final String createdAt;
  final String location;
  final String totals;

  OrderDetails(
      {Key key,
      this.id,
      this.status,
      this.createdAt,
      this.location,
      this.totals})
      : super(key: key);
  @override
  _OrderDetails createState() =>
      _OrderDetails(id, status, createdAt, location, totals);
}

class _OrderDetails extends State<OrderDetails> {
  final int id;
  final String status;
  final String createdAt;
  final String location;
  final String totals;

  _OrderDetails(
      this.id, this.status, this.createdAt, this.location, this.totals);
  var data;
  Map<String, dynamic> responseData;
  var token;
  String name;
  Color state;
  Future<List<OrdersDetails>> toggleId;
  final oCcy = new NumberFormat("#,##0.00", "en_US");
// get the token for the user
  void getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
      name = prefs.getString('name');
    });
  }
  // get the transaction

  Future<List<OrdersDetails>> _fetch3() async {
    final response = await http.get(
      baseUrl + 'transaction/$id',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      data = responseData['data']['orders'];
      print(data);
      final items = data.cast<Map<String, dynamic>>();
      List<OrdersDetails> listOfUsers = items.map<OrdersDetails>((json) {
        return OrdersDetails.fromJson(json);
      }).toList();
      return listOfUsers;
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

  void initState() {
    // TODO: implement initState
    super.initState();
    getToken();
    Timer(Duration(milliseconds: 100), () {
      setState(() {
        toggleId = _fetch3();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;
    final deviceHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: backgroundColor,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 60),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          height: 60,
                          width: 60,
                          //margin: EdgeInsets.only(top: 20),
                          // padding: EdgeInsets.only(top: 20,),
                          child: Image.asset(
                            'images/back.png',
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      Text(
                        'Order ID ' + id.toString(),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Flex(direction: Axis.horizontal, children: [
              Expanded(
                child: FutureBuilder<List<OrdersDetails>>(
                    future: toggleId,
                    builder: (context, snapshot) {
                      if (!snapshot.hasData)
                        return Center(
                          child: Container(
                            width: double.infinity,
                            height: 35,
                            padding: EdgeInsets.symmetric(horizontal: 24.0),
                            child: Text(''),
                          ),
                        );
                      print(snapshot.data);
                      return Container(
                        height: deviceHeight / 2,
                        child: ListView(
                          //scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          children: snapshot.data
                              .map((feed) => InkWell(
                                    onTap: () {
//    if(id =='2'){
//    gotoRetail
//    (context, id: feed.id,
//    title: feed.name);
//    }
//    else if(id =='1'){
//    gotoMeatShare
//    (context, id: feed.id,
//    title: feed.name);
//    }
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(20),
                                      child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Container(
                                                  margin: EdgeInsets.only(
                                                    right: 10.0,
                                                  ),
                                                  height: 120.0,
                                                  width: 110.0,
                                                  decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                        image:
                                                            CachedNetworkImageProvider(
                                                                feed.image),
                                                        fit: BoxFit.cover),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            12.0),
                                                  ),
                                                ),
                                                Flexible(
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: <Widget>[
                                                      Container(
                                                        margin:
                                                            EdgeInsets.all(10),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: <Widget>[
                                                            Text(
                                                              feed.name,
                                                              style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 16.0,
                                                              ),
                                                            ),
                                                            Text(
                                                              '\u{20A6}' +
                                                                  oCcy.format(feed.amount),
                                                              style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 16.0,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            left: 10),
                                                        child: Text(
                                                          'Quantity:   ' +
                                                              feed.quantity
                                                                  .toString(),
                                                          style: TextStyle(
                                                            fontSize: 12.0,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ]),
                                    ),
                                  ))
                              .toList(),
                        ),
                      );
                    }),
              ),
            ]),
            Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Drop-Off Location',
                      style: TextStyle(color: Colors.black, fontSize: 16),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      location,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      'Total',
                      style: TextStyle(fontSize: 16),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      totals != null ? '\u{20A6}' + oCcy.format(int.parse(totals)) : '0',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      'Created on',
                      style: TextStyle(fontSize: 16),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      createdAt,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                  ],
                )),
            SizedBox(
              height: 15,
            ),
            Container(
                margin: EdgeInsets.only(left: 10),
                height: 25,
                child: FlatButton(
                    textColor: colorAsh,
                    child: Text(
                      status,
                      style: TextStyle(color: Color(0xff7D650E)),
                    ),
                    color: Color(0xFFB08B00).withOpacity(.25),
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(18.0),
                      //side: BorderSide(color: Colors.red)
                    ),
                    onPressed: () {
                      //   Navigator.of(context).pushNamed('/signup');
                    })),
          ],
        ),
      ),
    );
  }
}
