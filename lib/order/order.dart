import 'dart:async';
import 'dart:convert';

import 'package:farmcrowdyfoods/login/loginpageTwo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:farmcrowdyfoods/Engine/OrderPojo.dart';
import 'package:farmcrowdyfoods/Engine/httpDetails.dart';
import 'package:farmcrowdyfoods/colorconst.dart';
import 'package:farmcrowdyfoods/order/orderDetails.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:intl/intl.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

class Order extends StatefulWidget {
  @override
  _Order createState() => _Order();
}

class _Order extends State<Order> with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  var data;
  Map<String, dynamic> responseData;
  var token;
  String name;
  String id = 'user/ongoing';
  Color state;
  final oCcy = new NumberFormat("#,##0.00", "en_US");
// get the token for the user
  void getToken() async {
    setCurrentScreen();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
      name = prefs.getString('name');
      toggleId = _fetch3();
    });
  }
  //Analytics
  Future<void> setCurrentScreen() async {
    FirebaseAnalytics analytics = FirebaseAnalytics();
    await analytics.setCurrentScreen(
      screenName: 'Order Page',
      screenClassOverride: 'Order History',
    );
    print('setCartScreen succeeded');
  }
  // get the transaction
  Future<List<Orders>> _fetch3() async {
    final response = await http.get(
      baseUrl + 'transaction/$id',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      data = responseData['data'];
      print(data);
      final items = data.cast<Map<String, dynamic>>();
      List<Orders> listOfUsers = items.map<Orders>((json) {
        return Orders.fromJson(json);
      }).toList();
      return listOfUsers;
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

  Future<List<Orders>> toggleId;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getToken();

    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 10),
    );

    _animationController.addListener(() => setState(() {}));
    _animationController.repeat();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final percentage = _animationController.value * 100;
    final deviceWidth = MediaQuery.of(context).size.width;
    final deviceHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: backgroundColor,
      body: SingleChildScrollView(
        child: token == null
            ? Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                Widget>[
                Container(
                    padding: EdgeInsets.only(top: 60, left: 20),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                'Order',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20),
                              ),
                            ],
                          ),
                          Divider(
                            thickness: 1,
                          ),
                          Center(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(
                                  height: 150,
                                ),
                                Text(
                                  'Sign up or Log In',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                                Text(
                                  'to view your order',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  margin: EdgeInsets.all(20),
                                  width: deviceWidth,
                                  child: Material(
                                      borderRadius: BorderRadius.circular(7.0),
                                      color: meatUpTheme,
                                      elevation: 10.0,
                                      shadowColor: Colors.white70,
                                      child: MaterialButton(
                                        onPressed: () {
                                          Navigator.pushNamed(
                                              context, "/SignUp");
                                        },
                                        child: Text(
                                          'Get Started',
                                          style: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            fontSize: 12.0,
                                            color: Colors.white,
                                          ),
                                        ),
                                      )),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      new MaterialPageRoute(
                                          builder: (context) =>
                                              new LoginTwo(getToken: getToken)),
                                    );

                                    //Navigator.pushNamed(context, "/Login");
                                  },
                                  child: Text(
                                    'Log in',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: meatUpTheme,
                                      fontSize: 15,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          )
                        ]))
              ])
            : Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 60, left: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
//                      GestureDetector(
//                        onTap: () {
//                          Navigator.pop(context);
//                        },
//                        child: Container(
//                          height: 60,
//                          width: 60,
//                          //margin: EdgeInsets.only(top: 20),
//                          // padding: EdgeInsets.only(top: 20,),
//                          child: Image.asset(
//                            'images/back.png',
//                            fit: BoxFit.fill,
//                          ),
//                        ),
                            // ),
                            Text(
                              'Order',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20.0,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ToggleSwitch(
                        minWidth: 150.0,
                        initialLabelIndex: 0,
                        activeBgColor: Colors.white,
                        activeTextColor: Color(0xffCD6147),
                        inactiveBgColor: Color(0xffE3E3E1),
                        inactiveTextColor: Color(0xff9F9F9C),
                        labels: ['Ongoing', 'History'],
                        onToggle: (index) {
                          print('switched to: $index');
                          if (index == 0) {
                            id = 'user/ongoing';
                            setState(() {
                              toggleId = _fetch3();
                            });
                          } else if (index == 1) {
                            id = 'user/history';
                            setState(() {
                              toggleId = _fetch3();
                            });
                            _fetch3();
                          }
                        },
                      ),
                    ],
                  ),
                  Flex(direction: Axis.horizontal, children: [
                    Expanded(
                      child: FutureBuilder<List<Orders>>(
                          future: toggleId,
                          builder: (context, snapshot) {
                            if (!snapshot.hasData) {
                              return Column(children: <Widget>[
                                SizedBox(
                                  height: 300,
                                ),
                                Container(
                                  width: double.infinity,
                                  height: 40,
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 24.0),
                                  child: LiquidLinearProgressIndicator(
                                    value: _animationController.value,
                                    backgroundColor: Colors.white,
                                    valueColor:
                                        AlwaysStoppedAnimation(meatUpTheme),
                                    borderRadius: 12.0,
                                    center: Text(
                                      "${percentage.toStringAsFixed(0)}%",
                                      style: TextStyle(
                                        color: Colors.redAccent,
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ]);
                            }
                            //print(snapshot.data);
                            else if (snapshot.data.isNotEmpty) {
                              return Container(
                                height: deviceHeight / 1.2,
                                child: ListView(
                                  //scrollDirection: Axis.vertical,
                                  shrinkWrap: true,
                                  children: snapshot.data
                                      .map(
                                        (feed) => InkWell(
                                          onTap: () {
                                            gotoFull(context,
                                                id: feed.id,
                                                createdAt: feed.createdAt,
                                                status: feed.statusName,
                                                totals: feed.amount.toString(),
                                                location: feed.location);
                                          },
                                          child: Container(
                                              padding: EdgeInsets.all(22),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Text(
                                                        'Order ID  ' +
                                                            feed.id.toString(),
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 16),
                                                      ),
                                                      Text(
                                                        feed.amount != null
                                                            ? '\u{20A6}${oCcy.format(feed.amount)}'
                                                                .toString()
                                                            : 5000.toString(),
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight.w500,
                                                            fontSize: 16),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 20,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Flexible(
                                                        child: Text(
                                                          feed.location,
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 12),
                                                        ),
                                                      ),
                                                      Container(
                                                          height: 26,
                                                          child: FlatButton(
                                                              child: Text(
                                                                feed.statusName,
                                                                style: TextStyle(
                                                                    color: Color(
                                                                        0xff7D650E)),
                                                              ),
                                                              color: Color(
                                                                      0xFFB08B00)
                                                                  .withOpacity(
                                                                      0.45),
                                                              shape:
                                                                  RoundedRectangleBorder(
                                                                borderRadius:
                                                                    new BorderRadius
                                                                            .circular(
                                                                        18.0),
                                                                //side: BorderSide(color: Colors.red)
                                                              ),
                                                              onPressed: () {
                                                                //   Navigator.of(context).pushNamed('/signup');
                                                              })),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Text(
                                                    feed.dateTime,
                                                    style: TextStyle(
                                                        color: Colors.black45,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontSize: 12),
                                                  ),
                                                ],
                                              )),
                                        ),
                                      )
                                      .toList(),
                                ),
                              );
                            } else if (snapshot.hasError) {
                              return Column(
                                children: <Widget>[
                                  SizedBox(
                                    height: 100,
                                  ),
                                  Text(
                                    'No order yet',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Column(
                                    children: <Widget>[
                                      Text(
                                          "${'Once an order is placed all notification'}"),
                                      Text("${'can be found here'}"),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 100,
                                  ),
                                ],
                              );
                            } else
                              return Center(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    SizedBox(
                                      height: 250,
                                    ),
                                    Text(
                                      'No order yet',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Column(
                                      children: <Widget>[
                                        Text(
                                            "${'Once an order is placed all notification'}"),
                                        Text("${'can be found here'}"),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Container(
                                      margin: EdgeInsets.all(20),
                                      width: deviceWidth,
                                      child: Material(
                                          borderRadius:
                                              BorderRadius.circular(7.0),
                                          color: meatUpTheme,
                                          elevation: 10.0,
                                          shadowColor: Colors.white70,
                                          child: MaterialButton(
                                            onPressed: () {
                                              Navigator.pushNamed(
                                                  context, "/BottomNav");
                                            },
                                            child: Text(
                                              'Place Order',
                                              style: TextStyle(
                                                fontWeight: FontWeight.w800,
                                                fontSize: 12.0,
                                                color: Colors.white,
                                              ),
                                            ),
                                          )),
                                    )
                                  ],
                                ),
                              );
                          }),
                    ),
                  ]),
                ],
              ),
      ),
    );
  }

  gotoFull(BuildContext context,
      {int id, var totals, String location, String createdAt, String status}) {
    Navigator.push(
        context,
        PageRouteBuilder(
            transitionDuration: Duration(milliseconds: 500),
            pageBuilder: (_, __, ___) => OrderDetails(
                id: id,
                createdAt: createdAt,
                status: status,
                location: location,
                totals: totals)));
  }
}
