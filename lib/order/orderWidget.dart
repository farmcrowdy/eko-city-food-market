import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:farmcrowdyfoods/Engine/basketOrderPojo.dart';
import 'package:farmcrowdyfoods/Engine/httpDetails.dart';
import 'package:farmcrowdyfoods/basket/cart.dart';
import 'package:farmcrowdyfoods/colorconst.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:farmcrowdyfoods/basket/cart.dart';
int total;

class CartList extends StatefulWidget {
final Carts refreshCart;

  const CartList({Key key, this.refreshCart}) : super(key: key);

  @override
  _CartList createState() => _CartList( refreshCart);
}

class _CartList extends State<CartList> {
  final Carts refreshCart;
  _CartList(this.refreshCart);
  var data;
  var tot;
  Map<String, dynamic> responseData;
  var token;
  String name;
  String message;
  int weight;
  int _quantity;

  void add() {
    setState(() {
      _quantity++;
    });
  }

  void minus() {
    setState(() {
      if (_quantity != 0) _quantity--;
    });
  }

  // get the token for the user
  void getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
    });
  }
  Future<List<Basket>> refresh;
  void initState() {
    // TODO: implement initState
    super.initState();
    getToken();
    Timer(Duration(seconds: 2), () {
      setState(() {
        refresh = fetchBasket();
      });
    });
  }


  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Basket>>(
      future: refresh,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<Basket> data = snapshot.data;
          if (data.isNotEmpty){
          return _jobsListView(data);
        }
          else
          return Column(
            children: <Widget>[
              SizedBox(
                height: 100,
              ),
              Text('No order yet', style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 20,
              ),),
              SizedBox(
                height: 10,
              ),
              Text(
                  "${'Once an order is placed all notification can be found here'}"),
              SizedBox(
                height: 100,
              ),
              Container(
                margin: EdgeInsets.all(20),
                child: Material(
                    borderRadius: BorderRadius.circular(7.0),
                    color: meatUpTheme,
                    elevation: 10.0,
                    shadowColor: Colors.white70,
                    child: MaterialButton(
                      onPressed: () {
                        Navigator.pushNamed(context, "/BottomNav");
                      },
                      child:

                      Text(
                        'Place Order',
                        style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: 12.0,
                          color: Colors.white,
                        ),
                      ),
                    )),
              )
            ],
          );
        }
        
  else if (snapshot.hasError) {
          return
            Center(
              child:
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 100,
              ),
              Text('No order yet', style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 20,
              ),),
              SizedBox(
                height: 10,
              ),
          Text("${'Once an order is placed all notification can be found here'}"),
              SizedBox(
                height: 100,
              ),
              Container(
                margin: EdgeInsets.all(20),
                child: Material(
                    borderRadius: BorderRadius.circular(7.0),
                    color: meatUpTheme,
                    elevation: 10.0,
                    shadowColor: Colors.white70,
                    child: MaterialButton(
                      onPressed: () {
                        Navigator.pushNamed(context, "/BottomNav");
                      },
                      child:

                      Text(
                        'Place Order',
                        style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: 12.0,
                          color: Colors.white,
                        ),
                      ),
                    )),
              )
            ],
            ),
          );
        }

  return   Container(
          width: double.infinity,
          height: 35,
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          child: LiquidLinearProgressIndicator(
            backgroundColor: Colors.white,
            valueColor: AlwaysStoppedAnimation(meatUpTheme),
            borderColor: meatUpTheme,
            borderWidth: 5.0,
            center: Text(
              "Loading...",
              style: TextStyle(
                fontSize: 12.0,
                fontWeight: FontWeight.bold, color: Colors.amber
              ),
            ),
          ),
        );
      },
    );
  }

  Future<List<Basket>> fetchBasket() async {
    final response = await http.get(
      baseUrl + 'transaction/4'     ,
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      data = responseData['data']['orders'];
      for (int i = 0; i < data.length; i++) {
        //Start of The Bottleneck is here...
        tot = data[0]['price'];
        tot = tot + tot;
//print(tot);
//        total = total + total;
//        print(total.toString());
        //End of The Bottleneck
      }
      //print(data);
      final items = data.cast<Map<String, dynamic>>();
      List<Basket> listOfUsers = items.map<Basket>((json) {
        return Basket.fromJson(json);
      }).toList();
      return listOfUsers;
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

  Container _jobsListView(data) {
    return
      Container(
        height: 320.0,
        child:
        ListView.builder(
      shrinkWrap: true,
        physics: const AlwaysScrollableScrollPhysics(),
        itemCount: data.length,
        itemBuilder: (context, index) {
          weight=  data[index].weight;
        _quantity = data[index].quantity;
          return _tile(data[index].id, data[index].packageId,data[index].userId,data[index].weight,data[index].quantity,
            data[index].statusId,data[index].price,data[index].orderType,data[index].name);
        }));
  }

  Container _tile(var id, var packageId,var userId,var weight,var quantity,var statusId,var price,var orderType,var name ) => Container(
    padding: EdgeInsets.all(16),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 10.0, top: 10),
              height: 120.0,
              width: 110.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(
                      'images/boneless.png',
                    ),
                    fit: BoxFit.cover),
                borderRadius: BorderRadius.circular(12.0),
              ),
            ),

            Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 10, right: 10),
                      child: Row(
                        mainAxisAlignment:
                        MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                           name,
                            style: TextStyle(
                              fontWeight: FontWeight.w800,
                              fontSize: 20.0,
                              color: Colors.black,
                            ),
                          ),
                          Text(
                            '$price',
                            style: TextStyle(
                              fontWeight: FontWeight.w800,
                              fontSize: 20.0,
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    ),

                    SizedBox(
                      height: 5,
                    ),

                    Container(
                      padding: EdgeInsets.only(
                        left: 16,
                      ),
                      child: '$orderType' == '1'?
                      Text(
                          ''
                      ): Row(
                        children: <Widget>[
                          SizedBox(
                            width: 5,
                          ),

                          DropdownButton<String>(
                            value: weight.toString(),
                            onChanged: (String newValue) {
                              setState(() {
                                weight = newValue;
                              });
                            },
                            items: <String>[
                              '1',
                              '2',
                              '3',
                              '5',
                              '8',
                            ].map<DropdownMenuItem<String>>(
                                    (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                          ),
                          Text(
                            'kg', style: TextStyle(
                              fontSize: 20
                          ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 10, right: 10),
                        child: Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: <Widget>[
                                    InkWell(
                                      onTap: (){
                                        print(_quantity - 1);
                                        setState(() {
                                          refresh = fetchBasket();
                                          _quantity = (_quantity -1);
                                        });

                                      },
                                      child:
                                      Container(
                                      width: 30.0,
                                      height: 30.0,
                                      child: new FloatingActionButton(
                                        heroTag:  Random(3),
                                        elevation: 0,
                                        onPressed:(){

                                         // minus();
                                        },
                                        child: new Icon(
                                          const IconData(0xe15b,
                                              fontFamily:
                                              'MaterialIcons'),
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                        backgroundColor: Colors.white,
                                      ),
  ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                      width: 10,
                                    ),
                                    new Text(_quantity.toString(),
                                        style: new TextStyle(
                                            fontSize: 20.0)),
                                    SizedBox(
                                      height: 10,
                                      width: 10,
                                    ),
                                    Container(
                                      width: 30.0,
                                      height: 30.0,
                                      child: new FloatingActionButton(
                                        heroTag: Random(3),
                                        elevation: 0,
                                        onPressed: add,
                                        child: new Icon(
                                          Icons.add,
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                        backgroundColor: Colors.white,
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                            InkWell(
                              onTap: (){
                                //Update the user profile
                               void   delete() async {
                                 print('hello');
                                  var header = {
                                    'Accept': 'application/json',
                                    'Authorization': 'Bearer $token'
                                  };
                                  print(header);
                                  final response = await http.delete(baseUrl + 'order/delete/$id',
                                    headers: header,);
                                  if (response.statusCode == 200) {
                                    setState(() {
                                      refresh = fetchBasket();
                                      setState(() {
                                        basketSize = (basketSize - 1);
                                      });
                                      //fetchData();
                                    });
                                    responseData = json.decode(response.body);
                                    message = responseData["message"];
                                    data = responseData["data"];
                                    print(responseData);
                                    print(message);
                                    Fluttertoast.showToast(msg: message);
                                  }
                                  {
                                    responseData = json.decode(response.body);
                                    var er = responseData['message'];
                                    print('error$er');
                                    Fluttertoast.showToast(
                                      msg: er,
                                      toastLength: Toast.LENGTH_LONG,
                                      timeInSecForIosWeb: 3,
                                      backgroundColor: meatUpTheme,
                                    );
                                  }
                                }
  showDialog(context: context, builder: (context)
                               {
                                 return AlertDialog(
                                   title: Text("Delete item from cart",),
                                   content: Text(
                                     "Are you sure you want to delete this item from your cart?",),
                                   actions: <Widget>[
                                     FlatButton(child: Text(
                                       "Cancel",
                                       style: TextStyle(
                                           color: Colors.white, fontSize: 16),
                                     ),
                                       onPressed: () => Navigator.pop(context),
                                       color: Colors.grey,),
                                     FlatButton(
                                       child: Text(
                                         "Delete",
                                         style: TextStyle(
                                             color: meatUpTheme, fontSize: 16),
                                       ),
                                       onPressed: () {
                                         Navigator.pop(context);
                                         //this.widget.callback((farmcart.sponsorUnit * farmcart.farmDetails.pricePerunit), false, farmcart.farmDetails.id);
                                         delete();
                                       },
                                     )
                                   ],
                                 );
                               });
  },
                              child:
                            Image.asset(
                              'images/deleteicon.png',
                              fit: BoxFit.fill,
                              height: 20,
                              width: 20,
                            ),
                            ),
                          ],
                        )),
                  ],
                )),
          ],
        ),
        Divider(),
      ],
    ),
  );

}