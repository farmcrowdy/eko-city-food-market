import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:farmcrowdyfoods/Engine/MeatPojo.dart';
import 'package:farmcrowdyfoods/Engine/httpDetails.dart';
import 'package:farmcrowdyfoods/colorconst.dart';
import 'package:farmcrowdyfoods/categorySelect/meatshare.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:intl/intl.dart';
import 'package:firebase_analytics/firebase_analytics.dart';


class FullOrder extends StatefulWidget {
  final int id;
  final String image;
  final String title;

  FullOrder({Key key, this.id, this.image, this.title}) : super(key: key);

  @override
  _AlmostThere createState() => _AlmostThere(id, image,title);
}

class _AlmostThere extends State<FullOrder> with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  int id;
  String image;
  String title;
  _AlmostThere(this.id, this.image, this.title);
  var data;
  Map<String, dynamic> responseData;
  var token;
  String name;
  final oCcy = new NumberFormat("#,##0.00", "en_US");
  String description;
  String shortDescription;
  var imageHeight;
  String phoneNo;
  String email;

  void getToken() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
      email = prefs.getString('email');
      phoneNo = prefs.getString('phoneNo');
    });
  }
// Per Analytics
  Future<void> _sendAnalyticsPackageSelected(String package) async {
    FirebaseAnalytics analytics = FirebaseAnalytics();

    await analytics.logEvent(
      name: 'Package Selected',
      parameters: <String, dynamic>{
        'Package': '$package',
        'time': DateTime.now().toString(),
        'phoneNo': phoneNo,
        'email':email,
      },
    );
    await analytics.logSelectContent(
      contentType: '$package',
      itemId: '$package',
    );

      await analytics.setCurrentScreen(
        screenName: 'Package Page',
        screenClassOverride: 'Update Profile',
      );

    print('logPackage Selected succeeded');

  }

  Future<List<Meats>> _fetchData() async {
    final response = await http.get(
      baseUrl + 'package/type/$id/limit/',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      data = responseData['data'];
      print(data);
      final items = data.cast<Map<String, dynamic>>();
      List<Meats> listOfUsers = items.map<Meats>((json) {
        return Meats.fromJson(json);
      }).toList();
      return listOfUsers;
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }
  Future<List<Meats>> toggleId;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    toggleId = _fetchData();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 10),
    );
    _animationController.addListener(() => setState(() {}));
    _animationController.repeat();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    final percentage = _animationController.value * 100;
    final deviceWidth = MediaQuery
        .of(context)
        .size
        .width;

    final deviceHeight = MediaQuery
        .of(context)
        .size
        .height;

imageHeight = deviceHeight/4;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: backgroundColor,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
        Hero(
        tag: 'home'+ id.toString(),
        child:
                Container(
                  height: imageHeight,
                  width:deviceWidth ,
                  child:
                  Image(
                    image:
                    CachedNetworkImageProvider(image,

                    ), fit: BoxFit.fill,
                  ),
                ),
        ),
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    height: 60,
                    width: 60,
                    margin: EdgeInsets.only(top: 30),
                    // padding: EdgeInsets.only(top: 20,),
                    child: Image.asset(
                      'images/back.png',
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomLeft,
                  child:
                  Container(
                    //margin: EdgeInsets.all(10),
                    padding: EdgeInsets.only(top: imageHeight-30, left: 20),
                    //padding: EdgeInsets.all(30),
                    child:
                    Text(
                      title,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            ),

            Flex(
              direction: Axis.horizontal,
              children: [
                Expanded(
                  child: FutureBuilder<List<Meats>>(
                      future: toggleId,
                      builder: (context, snapshot) {
                        if (!snapshot.hasData)
                          return
                            Center(
                              child:
                              Column(
                                  children: <Widget>[
                                    SizedBox(
                                      height: deviceHeight/7,
                                    ),
                                    Container(
                                      width: double.infinity,
                                      height: 40,
                                      padding: EdgeInsets.symmetric(horizontal: 24.0),
                                      child:
                                      Hero(
                                        tag: 'button',
                                        child:
                                        LiquidLinearProgressIndicator(
                                        value: _animationController.value,
                                        backgroundColor: Colors.white,
                                        valueColor: AlwaysStoppedAnimation(meatUpTheme),
                                        borderRadius: 12.0,
                                        center: Text(
                                          "${percentage.toStringAsFixed(0)}%",
                                          style: TextStyle(
                                            color: Colors.redAccent,
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ),
                                    ),
                                  ]
                          ),);
                        return  Container(
                            height: deviceHeight- imageHeight,
                            child:
                         ListView(
                          //scrollDirection: Axis.vertical,
                          //shrinkWrap: true,
                          children: snapshot.data
                              .map(
                                (feed) => InkWell(
                                  onTap: () {
                                    _sendAnalyticsPackageSelected( feed.name);
                                    gotoMeatShare
                                        (context, id: feed.id,
                                          title: feed.name,
                                      image: feed.image,
                                      categories:title,
                                      discountPrice:feed.discount != 0 && feed.discount != null?((feed.price) - ((feed.price * feed.discount)/100)): 0.0,
                                      );
                                  },
                                  child: Container(
                                    padding: EdgeInsets.only(left: 20, top: 20),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                    Hero(
                                    tag: 'hello'+ feed.id.toString(),
                                    child:
                                            Container(
                                              margin:
                                                  EdgeInsets.only(right: 10.0),
                                              height: 100.0,
                                              width: 100.0,
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image:
                                                    CachedNetworkImageProvider(feed.image),
                                                    fit: BoxFit.cover),
                                                borderRadius:
                                                    BorderRadius.circular(12.0),
                                              ),
                                            ),
                                    ),
                                            Flexible(
                                                child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  feed.name != null ?
                                                  '${feed.name}':'',
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w800,
                                                    fontSize: 18.0,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                feed.discount != 0 && feed.discount != null?
                                                Text(
                                                  feed.price  != null ?
                                                  '\u{20A6}${ oCcy.format(((feed.price) - ((feed.price * feed.discount)/100)))}':'',
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w800,
                                                    fontSize: 15.0,
                                                    color: Colors.blueGrey,
                                                  ),
                                                ):  Text(
                                                  feed.price  != null ?
                                                  '\u{20A6}${ oCcy.format(feed.price)}':'',
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w800,
                                                    fontSize: 15.0,
                                                    color: Colors.blueGrey,
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                              feed.discount != 0 && feed.discount != null?
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                Text(
                                                  feed.price  != null ?
                                                  '\u{20A6}${ oCcy.format(feed.price)}':'',
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    fontSize: 15.0,
                                                    decoration: TextDecoration.lineThrough,
                                                    color: Colors.blueGrey,
                                                  ),
                                                ),

                                                          Container(
                                                            height: 30,
                                                            width: 80,
                                                            decoration: new BoxDecoration(
                                                                color: discountPink2,
                                                                borderRadius: new BorderRadius.only(
                                                                    topLeft:  const  Radius.circular(40.0),
                                                                    bottomLeft: const  Radius.circular(40.0))
                                                            ),
                                                            child:
                                                            Center(
                                                                child:
                                                                Text(feed.discount != null ? '-${feed.discount.toString()} % OFF': '', style: TextStyle(color: discountPink, fontWeight: FontWeight.bold,),)
                                                            ),
                                                          )

                                                  ]

                                                ): Container(),

                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Text(
                                                  feed.shortDescription != null ?
                                                  feed.shortDescription: '',
                                                  style: TextStyle(
                                                    fontSize: 14.0,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                              ],
                                            )),
                                          ],
                                        )

                                      ],
                                    ),
                                  ),
                                ),
                              ).toList(),
                         )
                        );
                      }),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
  // gotoRetail(BuildContext context, {int id, String title, String description}) {
  //   Navigator.push(context, PageRouteBuilder(transitionDuration: Duration(milliseconds: 500),
  //       pageBuilder: (_,__,___) => RetailOrder(id: id, title: title)
  //   ));
  // }
  gotoMeatShare(BuildContext context, {int id, String title, String image, String categories, double discountPrice}) {
    Navigator.push(context,PageRouteBuilder(transitionDuration: Duration(milliseconds: 500),
        pageBuilder: (_,__,___) =>MeatShareOrder(id: id, title: title, image :image, categories:categories, discountPrice:discountPrice)
    ));
  }
}
