import 'dart:async';
import 'dart:convert';
import 'package:farmcrowdyfoods/login/loginpageTwo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:farmcrowdyfoods/Engine/httpDetails.dart';
import 'package:farmcrowdyfoods/basket/cart.dart';
import 'package:farmcrowdyfoods/colorconst.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_indicator_button/progress_button.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:intl/intl.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

List<String> images = [
  'images/basketpic.png',
  'images/asun.png',
];

class MeatShareOrder extends StatefulWidget {
  final int id;
  final String title;
  final String image;
  final String categories;
  final double discountPrice;
  MeatShareOrder(
      {Key key,
      this.id,
      this.title,
      this.image,
      this.categories,
      this.discountPrice})
      : super(key: key);
  @override
  _AlmostThere createState() =>
      _AlmostThere(id, title, image, categories, discountPrice);
}

class _AlmostThere extends State<MeatShareOrder> {
  int id;
  String title;
  String image;
  String categories;
  final double discountPrice;

  _AlmostThere(
      this.id, this.title, this.image, this.categories, this.discountPrice);

  int _quantity = 0;
  String description;
  var price = 0;
  int discount;
  String location;
  var locations = List();
  var data;
  String message;
  Map<String, dynamic> responseData;
  var token;
  var userId;
  var amount = 0;
  var basketNum;
  var slashedPrice;
  final oCcy = new NumberFormat("#,##0.00", "en_US");

  void add() {
    setState(() {
      _quantity++;
    });
  }

  void minus() {
    setState(() {
      if (_quantity != 0) _quantity--;
    });
  }

// get the token for the user
  getToken() async {
    FirebaseAnalytics analytics = FirebaseAnalytics();
    await analytics.setCurrentScreen(
      screenName: 'Add to cart Page',
      screenClassOverride: 'Add to cart',
    );

    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
      userId = prefs.getString('userId');
    });
  }

  fetchData() async {
    final response = await http.get(
      baseUrl + 'basket/size',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      basketNum = responseData['data'];
      setState(() {
        basketSize = basketNum;
      });
      print(basketSize);
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

  // get all the cart data
  void fetchCart() async {
    final response = await http.get(
      baseUrl + 'basket/summary',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      subTotal = responseData['data']['subtotal'];
      total = responseData['data']['total'];
      containerCharge = responseData['data']['container_charges'];
      vat = responseData['data']['delivery_charges'];
      basketSize = responseData['data']['basket_size'];

      Navigator.pushReplacementNamed(context, '/BottomNav');
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      Navigator.pushReplacementNamed(context, '/BottomNav');
      throw Exception('Failed to load internet');
    }
  }

  // get  the basket data
  fetchBasket() async {
    final response = await http.get(
      baseUrl + 'package/$id',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      data = responseData['data'];
      print(data);
      setState(() {
        description = data['description'];
        price = data['price'];
        discount = responseData['data']['discount'];
      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

//Analytics

  Future<void> addToCart() async {
    FirebaseAnalytics analytics = FirebaseAnalytics();
    await analytics.logAddPaymentInfo();
    await analytics.logAddToCart(
      currency: 'NGN',
      value: price.toDouble(),
      itemId: '$title',
      itemName: '$title',
      itemCategory: '$categories',
      quantity: _quantity,
      price: amount.toDouble(),
      origin: 'Farmcrowdy Foods',
      startDate: DateTime.now().toString(),
    );
    print('Cart Logged');
  }

  //Api to add to the basket

  createOrder(
      BuildContext context, int type, AnimationController controller) async {
    addToCart();
    controller.forward();
    var body = {
      'user_id': userId.toString(),
      'package_id': id.toString(),
      'quantity': _quantity.toString(),
      'weight': '1',
      'status_id': '3'
    };

    var header = {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };
    print(body);
    print(header);
    final response =
        await http.post(baseUrl + 'order/create', headers: header, body: body);
    if (response.statusCode == 200) {
      controller.reverse();
      fetchCart();
      responseData = json.decode(response.body);
      message = responseData["message"];
      data = responseData["data"];
      print(responseData);
      print(body);
      print(message);
      Fluttertoast.showToast(msg: message);
    } else {
      controller.reverse();
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      Fluttertoast.showToast(
        msg: er,
        toastLength: Toast.LENGTH_LONG,
        timeInSecForIosWeb: 3,
        backgroundColor: meatUpTheme,
      );
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchData();
    getToken();
    Timer(Duration(milliseconds: 100), () {
      fetchBasket();
    });
  }

  @override
  Widget build(BuildContext context) {
    final deviceHeight = MediaQuery.of(context).size.height;

    discount == 0
        ? amount = (_quantity * price)
        : amount = (_quantity * discountPrice.floor());
    final deviceWidth = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: backgroundColor,
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 35,
              ),
              Stack(
                children: <Widget>[
                  Hero(
                    tag: 'hello' + id.toString(),
                    child: Container(
                      height: deviceHeight / 4,
                      width: deviceWidth,
                      child: Image(
                        image: CachedNetworkImageProvider(
                          image,
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  discount == 0
                      ? Text('')
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: deviceHeight / 7,
                            ),
                            Container(
                              height: 40,
                              width: 80,
                              margin: EdgeInsets.only(top: 30),
                              decoration: new BoxDecoration(
                                  color: discountPink2,
                                  borderRadius: new BorderRadius.only(
                                      topRight: const Radius.circular(40.0),
                                      bottomRight:
                                          const Radius.circular(40.0))),
                              child: Center(
                                  child: Text(
                                discount != null
                                    ? '-${discount.toString()} % OFF'
                                    : '',
                                style: TextStyle(
                                  color: discountPink,
                                  fontWeight: FontWeight.bold,
                                ),
                              )),
                            )
                          ],
                        ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      height: 60,
                      width: 60,
                      margin: EdgeInsets.only(top: 30),
                      child: Image.asset(
                        'images/back.png',
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          title != null ? title : '',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 15),
                        ),
                        Row(
                          children: [
                            Text(
                              discount != 0
                                  ? '\u{20A6}' + oCcy.format(price)
                                  : '',
                              style: TextStyle(
                                  color: Colors.grey,
                                  decoration: TextDecoration.lineThrough,
                                  fontSize: 15),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Text(
                              discount == 0 || discount == null
                                  ? '\u{20A6}' + oCcy.format(price)
                                  : '\u{20A6}' + oCcy.format(discountPrice),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15),
                            ),
                          ],
                        )
                      ],
                    ),
                    Text(
                      description != null ? description : "",
                      style: TextStyle(color: Colors.black45, fontSize: 16),
                    ),
                    Divider(),
                    Text(
                      'Select Order Details',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 12),
                    ),
                  ],
                ),
              ),
              Container(
                color: Color(0xFFE4E4E4),
                padding: EdgeInsets.all(20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                              text: 'Quantity',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                              children: [
                                TextSpan(
                                    text: ' *',
                                    style: TextStyle(
                                      color: Colors.red,
                                    ))
                              ]),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 60.0,
                          height: 60.0,
                          child: new FloatingActionButton(
                            heroTag: 'minus',
                            onPressed: minus,
                            elevation: 0,
                            child: new Icon(
                              Icons.remove,
                              color: Colors.black,
                            ),
                            backgroundColor: Colors.white,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                          width: 30,
                        ),
                        new Text('$_quantity',
                            style: new TextStyle(fontSize: 18.0)),
                        SizedBox(
                          height: 10,
                          width: 30,
                        ),
                        Container(
                          width: 60.0,
                          height: 60.0,
                          child: new FloatingActionButton(
                            heroTag: 'add',
                            onPressed: add,
                            elevation: 0,
                            child: new Icon(
                              Icons.add,
                              color: Colors.black,
                            ),
                            backgroundColor: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 100,
                    ),
                  ]),
            ],
          ),
        ),
        bottomNavigationBar: Container(
          padding: EdgeInsets.all(20),
          height: 90,
          width: deviceWidth,
          child: ProgressButton(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            color: meatUpTheme,
            strokeWidth: 2,
            child: Row(
              children: <Widget>[
                Container(
                  height: 20,
                  width: 20,
                  child: Image.asset(
                    'images/addcart.png',
                  ),
                ),
                SizedBox(
                  width: 80,
                ),
                Text(
                  "Add $_quantity to Basket(\u{20A6}${oCcy.format(amount)})",
                  style: TextStyle(
                    fontWeight: FontWeight.w800,
                    fontSize: 12.0,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
            onPressed: (AnimationController controller) {
              if (token == null) {
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new LoginTwo(getToken: getToken)),
                );
                // Navigator.pushNamed(context, "/LoginTwo");
              } else
                _quantity != 0
                    ? createOrder(
                        context,
                        1,
                        controller,
                      )
                    : Fluttertoast.showToast(msg: 'Quantity can\'t be 0');
            },
          ),
        ));
  }
}
