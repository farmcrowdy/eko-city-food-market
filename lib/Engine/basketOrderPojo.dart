class Basket {

  var  id;
  var  packageId;
  var userId;
  var weight;
  var quantity;
  var statusId;
  var price;
  var orderType;
  String name;
  var image;
  int discountPrice;

  Basket({
    this.id,
    this.packageId,
    this.userId,
    this.weight,
    this.quantity,
    this.statusId,
    this.price,
    this.name,
    this.orderType,
    this.image,
    this.discountPrice
  });

  Basket.fromJson(Map<String, dynamic> doc):
        id =  doc['id'],
        packageId =  doc['package_id'],
        userId =  doc['user_id'],
        weight =  doc['weight'],
        quantity =  doc['quantity'],
        statusId =  doc['status_id'],
        price =  doc['price'],
        orderType = doc['order_type_id'],
        name =  doc['name'],
  image= doc['image'],
  discountPrice =  doc['discounted_price'];



}
