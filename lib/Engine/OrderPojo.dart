class Orders {

  int  id;
  int  userId;
  int statusId;
  String pickupLocationId;
  int paymentId;
  String updatedAt;
  String createdAt;
  String status;
  String statusName;
  String dateTime;
  String location;
  var amount;

  Orders({
    this.id,
    this.userId,
    this.statusId,
    this.createdAt,
    this.updatedAt,
    this.pickupLocationId,
    this.paymentId,
    this.status,
    this.statusName,
    this.dateTime,
    this.location,
    this.amount
  });

  Orders.fromJson(Map<String, dynamic> doc):
        id =  doc['id'],
        userId =  doc['user_id'],
        statusId =  doc['status_id'],
        updatedAt = doc['updated_at'],
        pickupLocationId = doc['pickup_location_id'],
        createdAt = doc['created_at'],
  paymentId = doc['payment_id'],
  statusName = doc['statusName'],
        dateTime = doc['dateTime'],
        location = doc['location'],
        amount = doc['amount'],
  status = doc['status'];
}
