import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:farmcrowdyfoods/login/loginpageTwo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:farmcrowdyfoods/Engine/httpDetails.dart';
import 'package:farmcrowdyfoods/colorconst.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:uuid_enhanced/uuid.dart';
import 'package:image/image.dart' as Im;
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

class Profile extends StatefulWidget {
  @override
  _Pickup createState() => _Pickup();
}

class _Pickup extends State<Profile> with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  Map<String, dynamic> responseData;
  var locations = List();
  String message;
  var data;
  var token;
  String email;
  File file;
  bool isUploading = false;
  String postId =new Uuid.randomUuid().toString();
  final DateTime timestamp = DateTime.now();
  String fileName;
  String dp;
//  FormData avatar;
  void getToken() async
  {
    setCurrentScreen();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
      email = prefs.getString('email');
      fetchDp();
    });
  }
  //Analytics
  Future<void> setCurrentScreen() async {
    FirebaseAnalytics analytics = FirebaseAnalytics();
    await analytics.setCurrentScreen(
      screenName: 'Profile Page',
      screenClassOverride: 'Profile Page',
    );
    print('setCartScreen succeeded');
  }
  fetchDp() async {
    final response = await http.get(
      baseUrl + 'user/profile/avatar',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);

      setState(() {
        dp = responseData['data']['avatar'];
      });
      print(dp);
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
    }
  }

  logout() async {
      var header =
      {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token'
      };

      print(header);
      final response = await http.post(
          baseUrl + 'logout',
          headers:header);
      if (response.statusCode == 200) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.remove('token');
        Navigator.pushReplacementNamed(context, "/SignUp");
        responseData = json.decode(response.body);
        message = responseData["message"];
        data = responseData["data"];
        print(responseData);
        print(message);
        Fluttertoast.showToast(msg: message);
      }
      {
        responseData = json.decode(response.body);
        var er = responseData['message'];
        print('error$er');
        Fluttertoast.showToast(msg: er, toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 3,
          backgroundColor: meatUpTheme,);
        Navigator.pushReplacementNamed(context, "/SignUp");
      }
    }

  handleTakePhoto() async {
    Navigator.pop(context);
    File file = await ImagePicker.pickImage(
        source: ImageSource.camera, maxWidth: 960, maxHeight: 675);
    setState(() {
      this.file = file;
      _startUploading();
    });
  }

  handleChooseFromGallery() async {

    Navigator.pop(context);
    File file = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxWidth: 960, maxHeight: 675);
    setState(() {
       fileName = file.path.split('/').last;
      this.file = file;
       _startUploading();
    });
  }

  selectImage(parentContext) {
    return showDialog(
        context: parentContext,
        builder: (context) {
          return SimpleDialog(
            title: Text('Upload Display Picture'),
            children: <Widget>[
              SimpleDialogOption(
                child: Text('Photo with Camera'),
                onPressed: handleTakePhoto,
              ),
              SimpleDialogOption(
                child: Text('Image from Gallery'),
                onPressed: handleChooseFromGallery,
              ),
              SimpleDialogOption(
                child: Text('Cancel'),
                onPressed: () => Navigator.pop(context),
              )
            ],
          );
        });
  }

  compressImage() async {
    final tempDir = await getTemporaryDirectory();
    final path = tempDir.path;
    Im.Image imageFile = Im.decodeImage(file.readAsBytesSync());
    final compressedImageFile = File('$path/"$timestamp.jpg')
      ..writeAsBytesSync(Im.encodeJpg(imageFile, quality: 85));
    setState(() {
      file = compressedImageFile;
    });
  }

//  uploadPicture() async {
//    var header =
//    {
//      'Accept': 'application/json',
//      'Authorization': 'Bearer $token'
//    };
//    var uri = Uri.parse(baseUrl + 'user/profile/image',);
//    var request = http.MultipartRequest('POST', uri)
//      ..fields['user'] = 'nweiz@google.com'
//      ..files.add(await http.MultipartFile.fromPath(
//          'package', 'build/package.tar.gz',
//          contentType: MediaType('application', 'x-tar')));
//    var response = await request.send();
//    if (response.statusCode == 200) print('Uploaded!');
//  }

  Future<Map<String, dynamic>> _uploadImage(File image) async {
    var header =
    {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };
    setState(() {
      isUploading = true;
    });
    // Find the mime type of the selected file by looking at the header bytes of the file
    final mimeTypeData =
    lookupMimeType(image.path, headerBytes: [0xFF, 0xD8]).split('/');
    // Intilize the multipart request
    final imageUploadRequest =
    http.MultipartRequest('POST', Uri.parse(baseUrl + 'user/profile/image'));
    imageUploadRequest.headers.addAll(header);
    // Attach the file in the request
    final file = await http.MultipartFile.fromPath(
        'avatar', image.path,

        contentType: MediaType(mimeTypeData[0], mimeTypeData[1]));
    // Explicitly pass the extension of the image with request body
    // Since image_picker has some bugs due which it mixes up
    // image extension with file name like this filenamejpge
    // Which creates some problem at the server side to manage
    // or verify the file extension
    imageUploadRequest.fields['ext'] = mimeTypeData[1];
    imageUploadRequest.files.add(file);

    try {
      final streamedResponse = await imageUploadRequest.send();
      final response = await http.Response.fromStream(streamedResponse);
      if (response.statusCode != 200) {
        return null;
      }
      final Map<String, dynamic> responseData = json.decode(response.body);
      _resetState();
      return responseData;
      //print(responseData);
    } catch (e) {
      print(e);
      return null;
    }
  }
  void _startUploading() async {
    final Map<String, dynamic> response = await _uploadImage(file);
    print(response);
    // Check if any error occured
    if (response == null || response.containsKey("error")) {
      print('error');
    } else {
      Fluttertoast.showToast(msg: 'You have successfully uploaded your Avatar', toastLength: Toast.LENGTH_LONG,
        timeInSecForIosWeb: 3,
        backgroundColor: meatUpTheme,);
     print('success');
    }
  }
  void _resetState() {
    setState(() {
      isUploading = false;
      //file = null;
    });
  }


  uploadPictures() async {
    print(fileName);
    await compressImage();
      var body = {
        //avatar : fileName,
      };

      var header =
      {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token'
      };
      print(body);
      print(header);
      final response = await http.post(
          baseUrl + 'user/profile/image',
          headers:header, body: body, );
      if (response.statusCode == 200) {
        //Navigator.pushReplacementNamed(context, "/Login");
        responseData = json.decode(response.body);
        message = responseData["message"];
        data = responseData["data"];
        print(responseData);
        print(body);
        print(message);
        Fluttertoast.showToast(msg: message.toString());
      }
      else
      {
        responseData = json.decode(response.body);
        var er = responseData['message'];
        print('error$er');
        Fluttertoast.showToast(msg: message.toString());
        Fluttertoast.showToast(msg: er, toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 3,
          backgroundColor: meatUpTheme,);
      }
  }

  // Alert dialog
  _showErrorDialog(
      {String message = ""}) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Error Logging in"),
            content: Text(message),
            actions: <Widget>[
              FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        }
    );
  }

    @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getToken();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 10),
    );

    _animationController.addListener(() => setState(() {}));
    _animationController.repeat();
    }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    final percentage = _animationController.value * 100;
    final deviceWidth = MediaQuery.of(context).size.width;
    return
      Scaffold(

          backgroundColor: backgroundColor,
          body:
          SingleChildScrollView(
            child:
            token == null?

            Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[

                  Container(
                      padding: EdgeInsets.only(top: 60, left: 20),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[

                                Text(
                                  'Profile',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                ),
                              ],
                            ),
                            Divider(
                              thickness: 1,
                            ),
                            Center(
                              child:
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  SizedBox(
                                    height: 150,
                                  ),
                                  Text(
                                    'Sign up or Log In',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                    ),
                                  ),
                                  Text(
                                    'to view your profile',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                    ),
                                  ),


                                  SizedBox(
                                    height: 20,
                                  ),
                                  Container(
                                    margin: EdgeInsets.all(20),
                                    width: deviceWidth,
                                    child: Material(
                                        borderRadius: BorderRadius.circular(7.0),
                                        color: meatUpTheme,
                                        elevation: 10.0,
                                        shadowColor: Colors.white70,
                                        child: MaterialButton(
                                          onPressed: () {
                                            Navigator.pushNamed(context, "/SignUp");
                                          },
                                          child:

                                          Text(
                                            'Get Started',
                                            style: TextStyle(
                                              fontWeight: FontWeight.w800,
                                              fontSize: 12.0,
                                              color: Colors.white,
                                            ),
                                          ),
                                        )),
                                  ),
                                  InkWell(
                                    onTap: (){
                                      Navigator.push(context, new MaterialPageRoute(builder: (context) => new LoginTwo(getToken:getToken)),);

                                    },
                                    child:
                                    Text(
                                      'Log in',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,color: meatUpTheme,
                                        fontSize: 15,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ]
                      ))
                ]
            ):
            Column(

              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                Container(
                  padding: EdgeInsets.only(top:60),
                  child:
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
Container(
                  padding: EdgeInsets.all(10),
  child:
                          Text(
                             'Profile',
                                style: TextStyle(
                                    color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20 ),
                                ),
                ),
                        ]
                      ),

                      Divider(),

                      Center(
                        child:

                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          file == null?
                      CircleAvatar(
                        radius: 30,
                        backgroundImage:  CachedNetworkImageProvider(dp != null?dp:''),
                      ):
                        Container(
                        width: 110.0,
                        height: 110.0,
                     child:
                        new CircleAvatar(backgroundImage: new FileImage(file),

                          )
                          ),

                        SizedBox(
                        height: 10,
                      ),
                       // selectImage(context)

GestureDetector(
  onTap: (){
    //handleChooseFromGallery();
    selectImage(context);
  },
  child: Text(

                        'Change my picture', style: TextStyle(
                        color: meatUpTheme, fontSize: 12
                      ),
                      ),
),

                        ],
                      ),
                      ),
SizedBox(
  height: 100,
),

InkWell(
  onTap: () => Navigator.pushNamed(context, "/ProfileDetails"),
  child:
Container(
  padding: EdgeInsets.all(20),
  child: 
                      Row(
                        children: <Widget>[
                          Container(
                            height: 36,
                            width: 36,
                            child:
                        Image(
                              image:
                          AssetImage('images/details.png'),),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Your Details', style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold
                              ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                'Name, Email address, Phone number....', style: TextStyle(
                                  fontSize: 12, color: Color(0XFF4A4D4E)
                              ),
                              )
                            ],
                          )


                        ],
                      )
),
),
                      InkWell(
                        onTap: () => Navigator.pushNamed(context, "/Security"),
                        child:

                      Container(
                          padding: EdgeInsets.all(20),
                          child:
                          Row(
                            children: <Widget>[
                              Container(
                                height: 36,
                                width: 36,
                                child:
                                Image(

                                  image:
                                  AssetImage('images/security.png'),),
                              ),
                              SizedBox(
                                width: 15,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[

                                  Text(
                                    'Security', style: TextStyle(
                                      fontSize: 16, fontWeight: FontWeight.bold
                                  ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),

                                  Text(
                                    'Change Password', style: TextStyle(
                                   fontSize: 12, color: Color(0XFF4A4D4E)
                                  ),
                                  )
                                ],
                              )
                            ],
                          )
                      ),
                      ),
            ],
          ),
          ),
        ]
      ),
          ),
          bottomNavigationBar:
          token == null?
          Container(
            height: 0,
            width: 0,
          ):
          Container(
            margin: EdgeInsets.all(20),
            child:
            Material(
                borderRadius: BorderRadius.circular(7.0),
                color: meatUpTheme,
                elevation: 10.0,
                shadowColor: Colors.white70,
                child: MaterialButton(
                  onPressed: (){
                    logout();
                  },
                  child:
                  Text(
                    'Logout',
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      fontSize: 12.0,
                      color: Colors.black,
                    ),
                  ),

                )

            ),

          )

      );
  }
}
