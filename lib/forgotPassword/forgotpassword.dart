import 'dart:convert';
import 'package:farmcrowdyfoods/Engine/httpDetails.dart';
import 'package:farmcrowdyfoods/colorconst.dart';
import 'package:farmcrowdyfoods/forgotPassword/verification.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_indicator_button/progress_button.dart';

class ResetPassword extends StatefulWidget {
  @override
  _SignUp createState() => _SignUp();
}

class _SignUp extends State<ResetPassword> {
  // Variables Declared
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController numberController = TextEditingController();
  Map<String, dynamic> responseData;
  String message;
  var data;

// Alert dialog
  _showErrorDialog({String message = ""}) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Error"),
            content: Text(message),
            actions: <Widget>[
              FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  //Http call
  register(
      BuildContext context, int type, AnimationController controller) async {
    //  prefs = await SharedPreferences.getInstance();
    controller.forward();
    var body = {
      "email": numberController.text,
    };
    print(body);
    final response = await http.post(baseUrl + 'password/reset/request',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: body);
    if (response.statusCode == 200) {
      controller.reverse();
      responseData = json.decode(response.body);
      message = responseData["message"];
      data = responseData["data"];
      print(responseData);
      print(message);
      Fluttertoast.showToast(msg: message);
      verify(context, phoneNo: numberController.text);
    } else {
      controller.reverse();
      // var er = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$responseData');
      _showErrorDialog(message: er);
    }
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;
    final deviceHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: backgroundColor,
      key: _scaffoldKey,
      body: ListView(children: <Widget>[
        Column(
          children: <Widget>[
            Stack(
              alignment: Alignment.bottomLeft,
              children: <Widget>[
                Container(
                  child: Image.asset(
                    'images/signup.png',
                    width: deviceWidth,
                    height: deviceHeight / 1.7,
                    fit: BoxFit.fill,
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(30),
                  height: 110,
                  width: 200,
                  child: Image.asset(
                    'images/foodlogo.png',
                    fit: BoxFit.fill,
                  ),
                ),
              ],
            ),
            Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 20, top: 30, bottom: 16),
                    child: Text(
                      'Reset with your email',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 20, right: 20, bottom: 30),
                    child: TextFormField(
                      controller: numberController,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.all(20.0),
                        labelStyle:
                            TextStyle(color: hintTextColor, fontSize: 12),
                        hintText: 'name@gmail.com',
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0),
                          borderSide: new BorderSide(),
                        ),
                      ),
                      keyboardType: TextInputType.emailAddress,
                      style: TextStyle(
                          color: activeTextColor,
                          fontSize: 12,
                          fontWeight: FontWeight.w600),
                      cursorColor: Colors.black,
                    ),
                  ),
                ]),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              height: 60,
              width: deviceWidth,
              child: ProgressButton(
                borderRadius: BorderRadius.all(Radius.circular(8.0)),
                color: meatUpTheme,
                strokeWidth: 2,
                child: Text(
                  "Reset your Account Password",
                  style: TextStyle(
                    fontWeight: FontWeight.w800,
                    fontSize: 12.0,
                    color: Colors.white,
                  ),
                ),
                onPressed: (AnimationController controller) {
                  register(
                    context,
                    1,
                    controller,
                  );
                },
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Container(
              padding: EdgeInsets.all(20),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Remembered? ",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 12,
                        fontWeight: FontWeight.normal),
                  ),
                  new GestureDetector(
                    onTap: () {
                      Navigator.pushReplacementNamed(context, "/Login");
                    },
                    child: new Text(
                      "Log in here",
                      style: TextStyle(
                        color: meatUpTheme2,
                        fontWeight: FontWeight.w600,
                        fontSize: 12,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        )
      ]),
    );
  }

  verify(BuildContext context, {String phoneNo}) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ResetVerifyPin(phoneNo: phoneNo);
    }));
  }
}
