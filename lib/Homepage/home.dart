import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:farmcrowdyfoods/Engine/MeatPojo.dart';
import 'package:farmcrowdyfoods/Engine/httpDetails.dart';
import 'package:farmcrowdyfoods/categorySelect/perCatergories.dart';
import 'package:flutter/scheduler.dart';
import 'package:farmcrowdyfoods/colorconst.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:shimmer/shimmer.dart';
import 'package:flutter_fab_dialer/flutter_fab_dialer.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

const String feature1 = 'feature1',
    feature2 = 'feature2',
    feature10 = 'feature10';
class HomePage extends StatefulWidget {
  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;
  HomePage({Key key, this.analytics, this.observer}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState(analytics, observer);
}
//ListWheelScrollView

class _MyStatefulWidgetState extends State<HomePage> {
  _MyStatefulWidgetState(this.analytics, this.observer);

  var data;
var topHeight;
  Map<String, dynamic> responseData;
  var token;
  String name;
  String whatsAppOne;
  String whatsAppTwo;
  String phoneNo;
  String email;

  final FirebaseAnalyticsObserver observer;
  final FirebaseAnalytics analytics ;
  String _message = '';

  void setMessage(String message) {
    setState(() {
      _message = message;
      print(_message);
    });
  }

  //Analytics
  Future<void> setCurrentScreen() async {
    FirebaseAnalytics analytics = FirebaseAnalytics();
    await analytics.setCurrentScreen(
      screenName: 'Home Page',
      screenClassOverride: 'Items Categories',
    );
    setMessage('setCurrentScreen succeeded');
  }
  Future<void> _sendAnalyticsFrontPage() async {
    FirebaseAnalytics analytics = FirebaseAnalytics();
    await analytics.logEvent(
      name: 'FrontPage_launch',
      parameters: <String, dynamic>{
        'name': '$name',
        'phoneNo': phoneNo,
        'email':email,
        'time': DateTime.now().toString(),
      },
    );
    setMessage('LogHomePage succeeded');
  }
  Future<void> setUserId() async {
    FirebaseAnalytics analytics = FirebaseAnalytics();
    await analytics.setUserId(email);
    setMessage('setUserId succeeded');
  }

  Future<void> _sendAnalyticsCategorySelected(String category) async {
    FirebaseAnalytics analytics = FirebaseAnalytics();

    await analytics.logEvent(
      name: 'Category Selected',
      parameters: <String, dynamic>{
        'category': '$category',
        'time': DateTime.now().toString(),
        'phoneNo': phoneNo,
        'email':email,
      },
    );
    setMessage('logCategory succeeded');
  }


// get the token for the user
  void getToken() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
      name = prefs.getString('name');
      email = prefs.getString('email');
      phoneNo = prefs.getString('phoneNo');
      setUserId();
      _sendAnalyticsFrontPage();
      setCurrentScreen();
    });
  }
  // get the last basket number



  _fetchWhatsAppLink() async {
    final response = await http.get(
      baseUrl + 'whatsapp_link',
      headers: {'Accept': 'application/json'},
    );

    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      whatsAppOne = responseData['data']['whatsapp_link'];
      print(whatsAppOne);
    }

    else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }



  Future<List<Meats>> _fetch3() async {
    final response = await http.get(
      baseUrl + 'category/all',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      data = responseData['data']['categories'];
      print(data);
      final items = data.cast<Map<String, dynamic>>();
      List<Meats> listOfUsers = items.map<Meats>((json) {
        return Meats.fromJson(json);
      }).toList();
      return listOfUsers;
    }

    else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

  Future<List<Meats>> toggleId;

  @override
  void initState() {

    SchedulerBinding.instance.addPostFrameCallback((Duration duration) {
      FeatureDiscovery.discoverFeatures(
        context,
        const <String>{
          // Feature ids for every feature that you want to showcase in order.
          feature1,
          feature2,
        },
      );
    });
    _fetchWhatsAppLink();
    // TODO: implement initState
    super.initState();
    getToken();

    toggleId = _fetch3();
  }


  launchWhatsApp() async {
    String url = whatsAppOne;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  launchWhatsApp2() async {
    String url = whatsAppTwo;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  var icon1;
  @override
  Widget build(BuildContext context) {
    final action = () async {
      print('IconButton of $feature1 tapped.');
      return true;
    };

    icon1 = Icon(
      Icons.shopping_cart,
      color: Colors.white,
      size: 30,
    );

    var _fabMiniMenuItemList = [

      new FabMiniMenuItem.withTextWithImage(
          AssetImage('images/whatsappIcon.jpg'),
          4.0, "Shop with Uduak",
          launchWhatsApp,
          "Shop with Uduak",
          meatUpTheme,
          Colors.white, true),
    ];


    final deviceWidth = MediaQuery.of(context).size.width;
    final deviceHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: backgroundColor,
        body:
        new Stack(
          children: <Widget>[

            ListView(
          children: <Widget>[
            Column(
              children: <Widget>[

                Container(
                 // height: deviceHeight/11,
                  child:

        Column(
        children: <Widget>[

                SizedBox(
                  height: 10,
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 20),
                      child: Text(
                        name != null?
                        'Hi $name,': '',
                        style:
                       TextStyle(
                            color: Colors.black45,
                            fontSize: 15,
                          ),
                        ),
                      ),

                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 20),
                      child: Text(
                        'What are you getting today?',
                        style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.w600),

                      ),
                    ),
                  ],
                ),
                ]
        ),
                ),
                SizedBox(
                  height: 20,
                ),
                Flex(direction: Axis.horizontal, children: [
                  Expanded(
                    child: FutureBuilder<List<Meats>>(
                        future: toggleId,
                        builder: (context, snapshot) {
                         if (!snapshot.hasData)
                            return
                              Container(
                                  padding: EdgeInsets.all(20.0),
                                  child:Center(
                                    child: Shimmer.fromColors(
                                        direction: ShimmerDirection.ltr,
                                        period: Duration(seconds:2),
                                        child: Column(
                                          children: [0, 1, 2, 3,4,5,6]
                                              .map((_) => Padding(
                                            padding: const EdgeInsets.only(bottom: 8.0),
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  width: deviceWidth-20,
                                                  height: 150,
                                                  color: Colors.white,
                                                ),
                                                Padding(
                                                  padding:
                                                  const EdgeInsets.symmetric(horizontal: 8.0),
                                                ),
                                                Expanded(
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Container(
                                                        width: double.infinity,
                                                        height: 8.0,
                                                        color: Colors.white,
                                                      ),
                                                      Padding(
                                                        padding:
                                                        const EdgeInsets.symmetric(vertical: 2.0),
                                                      ),
                                                      Container(
                                                        width: double.infinity,
                                                        height: 8.0,
                                                        color: Colors.white,
                                                      ),
                                                      Padding(
                                                        padding:
                                                        const EdgeInsets.symmetric(vertical: 2.0),
                                                      ),
                                                      Container(
                                                        width: 40.0,
                                                        height: 8.0,
                                                        color: Colors.white,
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ))
                                              .toList(),
                                        ),
                                        baseColor: meatUpTheme,
                                        highlightColor: Colors.white),
                                  )
                              );

//    return ListView.builder(
//    itemCount: 3,
//    // Important code
//    itemBuilder: (context, index) => Shimmer.fromColors(
//    baseColor: Colors.grey[400],
//    highlightColor: Colors.white,
//    child:
//    Flex(direction: Axis.horizontal, children: [
//    Expanded(
//      child:
//      Container(
//        height: 20,
//        width: 200,
//        color: Colors.grey,
//      ),
//    )]
//    ),
//    ),
//    );

                          return

                            Container(
                              height: deviceHeight/1.25,
                              child:
                              ListView(
                            //scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            children: snapshot.data
                                .map(
                                  (feed) => InkWell(
                                    onTap: () {
                                      _sendAnalyticsCategorySelected(feed.name);
                                      viewMore(context,
                                          id: feed.id,
                                      image:feed.image,
                                      title:feed.name);
                                      },

                                    child:
                                      Container(

                                      padding: EdgeInsets.only(left: 20, top: 20, right: 20),
                                      child:
                                      Stack(
                                        children: <Widget>[
                                      Hero(
                                      tag: 'home'+ feed.id.toString(),
                                        child:
                                          Container(
                                            width: deviceWidth,
                                            height: 150,
                                            decoration: BoxDecoration(
                                              color:  Colors.black.withOpacity(0.9),
                                              image: DecorationImage(
                                                colorFilter: new ColorFilter.mode(Colors.black
                                                    .withOpacity(0.6), BlendMode.dstATop),
                                          image:
                                          CachedNetworkImageProvider(feed.image,),
                                                  fit: BoxFit.cover,
                                            ),
                                            ),
                                          ),
                                      ),
                                        Align(
                                          alignment: Alignment.bottomLeft,
                                          child:
                                          Container(
                                            //margin: EdgeInsets.all(10),
                                            padding: EdgeInsets.only(top: 120, left: 10),
                                            child:
                                            Text(
                                                    '${feed.name}',
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w800,
                                                      fontSize: 18.0,
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                          ),
                                        ),
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                                .toList(),
                              ),
                          );
                        }),
                  ),
                ]),

              ],
            ),

          ],
        ),
    DescribedFeatureOverlay(
    featureId: feature1,
    tapTarget: icon1,targetColor: meatUpTheme,
    backgroundColor: meatUpTheme2,
    contentLocation: ContentLocation.below,
    title: const Text('Use a Personal Shopper'),
    description: const Text(
    'Stressed out from having to make a list and go to the market all the time? We\'ve create a new feature where you can have someone shop for you bi-weekly or monthly and stock up your home with groceries. Kindly click on the icon blinking to acknowlege the update.'),
    onComplete: action,
    onOpen: () async {
    print('The $feature1 overlay is about to be displayed.');
    return true;
    },
    child:
            new FabDialer(
              _fabMiniMenuItemList, meatUpTheme, Icon(Icons.shopping_cart,),),
    ),

  ]
        ),
    );
  }



  viewMore(BuildContext context, {int id, String image,String title}) {
    Navigator.push(context, PageRouteBuilder(transitionDuration: Duration(milliseconds: 500),
      pageBuilder: (_,__,___) =>
     FullOrder(id: id, image: image, title:title)));
    }
  }

