import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:farmcrowdyfoods/Engine/httpDetails.dart';
import 'package:farmcrowdyfoods/Homepage/bottmNav.dart';
import 'package:farmcrowdyfoods/Homepage/home.dart';
import 'package:farmcrowdyfoods/Profile/profile.dart';
import 'package:farmcrowdyfoods/Profile/security.dart';
import 'package:farmcrowdyfoods/Profile/yourDetails.dart';
import 'package:farmcrowdyfoods/basket/cart.dart';
import 'package:farmcrowdyfoods/Payment/pickup.dart';
import 'package:farmcrowdyfoods/forgotPassword/forgotpassword.dart';
import 'package:farmcrowdyfoods/forgotPassword/security.dart';
import 'package:farmcrowdyfoods/login/loginpage.dart';
import 'package:farmcrowdyfoods/login/loginpageTwo.dart';
import 'package:farmcrowdyfoods/categorySelect/perCatergories.dart';
import 'package:farmcrowdyfoods/categorySelect/meatshare.dart';
import 'package:farmcrowdyfoods/order/order.dart';
import 'package:farmcrowdyfoods/order/orderDetails.dart';
import 'package:farmcrowdyfoods/signup/almostThere.dart';
import 'package:farmcrowdyfoods/signup/signup.dart';
import 'package:farmcrowdyfoods/signup/verification.dart';
import 'package:farmcrowdyfoods/splashscreen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';


void main() => runApp(MyApp());


class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}
class _MyAppState extends State<MyApp> {
  var basketNum;
  var data;
  Map<String, dynamic> responseData;
  var token;
  String name;

  // get the token for the user
  void getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
    });
  }

// get all the cart data
  void fetchCart() async {
    final response = await http.get(
      baseUrl + 'basket/summary',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        subTotal = responseData['data']['subtotal'];
        total = responseData['data']['total'];
        containerCharge = responseData['data']['container_charges'];
        vat = responseData['data']['delivery_charges'];
        basketSize = responseData['data']['basket_size'];
      });

    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

//Get basket Number
  fetchData() async {
    final response = await http.get(
      baseUrl + 'basket/size',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      basketNum = responseData['data'];
      setState(() {
        basketSize = basketNum;
      });
      print(basketSize);
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      setState(() {
        basketSize = 0;
      });
      throw Exception('Failed to load internet');
    }
  }

  final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  new FlutterLocalNotificationsPlugin();
  void registerNotification() async {
    firebaseMessaging.getToken().then((token){
      print('My token:  '+token);
    });
    firebaseMessaging.requestNotificationPermissions();
    firebaseMessaging.configure(onMessage: (Map<String, dynamic> message) {
      print('onMessage: $message');
      showNotification(message['notification']);
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(15)),
          content: ListTile(
            title: Text(message['notification']['title']),
            subtitle: Text(message['notification']['body']),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        ),
      );
      return;
    }, onResume: (Map<String, dynamic> message) {
      print('onResume: $message');
      showNotification(message['notification']);
     // Navigator.of(context).pushNamed('/MyTabs');
      return;
    }, onLaunch: (Map<String, dynamic> message) {
      showNotification(message['notification']);
      //Navigator.of(context).pushNamed('/MyTabs');
      print('onLaunch: $message');
      return;
    });
  }

  void showNotification(message) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      Platform.isAndroid
          ? 'FLUTTER_NOTIFICATION_CLICK'
          : 'FLUTTER_NOTIFICATION_CLICK',
      'Flutter chat demo',
      'your channel description',
      playSound: true,
      enableVibration: true,
      importance: Importance.Max,
      priority: Priority.High,
    );
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(0, message['title'].toString(),
        message['body'].toString(), platformChannelSpecifics,
        payload: json.encode(message));
  }



  Widget _buildDialog(BuildContext context, dynamic message) {
    return AlertDialog(
      content: Text(message),
      actions: <Widget>[
        FlatButton(
          child: const Text('CLOSE'),
          onPressed: () {
            Navigator.pop(context, false);
          },
        ),
        FlatButton(
          child: const Text('SHOW'),
          onPressed: () {
            Navigator.pop(context, true);
          },
        ),
      ],
    );
  }

  void _showItemDialog(String message) {
    showDialog<bool>(
      context: context,
      builder: (_) => _buildDialog(context, message),
    ).then((bool shouldNavigate) {
      if (shouldNavigate == true) {
        //_navigateToDetail(message);
      }
    });
  }

  void initState() {
    // TODO: implement initState
    super.initState();
    getToken();
    registerNotification();
    Timer(Duration(milliseconds: 100), () {
      setState(() {
        fetchData();
        fetchCart();
      });
    });

  }

  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
  FirebaseAnalyticsObserver(analytics: analytics);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Farmcrowdy Foods',
      theme: ThemeData(fontFamily: 'Gordita'),
      debugShowCheckedModeBanner: false,
      navigatorObservers: <NavigatorObserver>[observer],
      home: AnimatedSplashScreen(),
      routes: <String,WidgetBuilder>{
        "/SignUp":(BuildContext context)=>new SignUp(),
        "/VerifyPin":(BuildContext context)=>new VerifyPin(),
        "/AlmostThere":(BuildContext context)=>new AlmostThere(),
        "/Login":(BuildContext context)=>new Login(),
        "/LoginTwo":(BuildContext context)=>new LoginTwo(),
        "/HomePage":(BuildContext context)=>new HomePage(),
        "/MeatShareOrder":(BuildContext context)=>new MeatShareOrder(),
        "/BottomNav":(BuildContext context)=> FeatureDiscovery(
          child:   BottomNav(),
        ),
        "/Pickup":(BuildContext context)=>new Pickup(),
        "/Order":(BuildContext context)=>new Order(),
        "/OrderDetails":(BuildContext context)=>new OrderDetails(),
        "/FullOrder":(BuildContext context)=>new FullOrder(),
        "/Cart":(BuildContext context)=>new Cart(),
        "/Profile":(BuildContext context)=>new Profile(),
        "/ProfileDetails":(BuildContext context)=>new ProfileDetails(),
        "/Security":(BuildContext context)=>new Security(),
        "/MyApp":(BuildContext context)=>new MyApp(),
        "/ResetPassword":(BuildContext context)=>new ResetPassword(),
        "/ResetPassField":(BuildContext context)=>new ResetPassField(),
      },
    );
  }

}