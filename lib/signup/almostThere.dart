import 'dart:convert';
import 'package:farmcrowdyfoods/Engine/httpDetails.dart';
import 'package:farmcrowdyfoods/signup/signup.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:farmcrowdyfoods/colorconst.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:farmcrowdyfoods/signup/verification.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_indicator_button/progress_button.dart';


class AlmostThere extends StatefulWidget {
  @override
  _AlmostThere createState() => _AlmostThere();
}

class _AlmostThere extends State<AlmostThere> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController cityController = TextEditingController();

Color btnColor;
  String dropdownValue;
  Map<String, dynamic> responseData;
  var locations = List();
  String _locationId;
  String message;
  var data;
  var token;
String gender = 'Select Gender';
  bool passwordVisible;

  // Alert dialog
  _showErrorDialog(
      {String message = ""}) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Error Signing up"),
            content: Text(message),
            shape:
            RoundedRectangleBorder(borderRadius: new BorderRadius.circular(15)),
            actions: <Widget>[
              FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        }
    );
  }

// Retrieve the token from the shared preference

  void getToken() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('verifytoken');
    });
  }

  // ignore: missing_return
  Future<String> _location() async {
    final response = await http.get(
        baseUrl + 'states/all',
        headers: {
          'Accept': 'application/json'});

    print(response.statusCode);
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        locations = responseData["data"];
        print(locations);
      });

    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$sa');
      print(er);
      throw Exception('Failed to load internet');
    }
  }

  createAccount( BuildContext context, int type, AnimationController controller) async {
    controller.forward();
 print(_locationId);
    if (_locationId != null) {
      var body = {
        'password': passwordController.text,
        'state': _locationId,
        'phone_number':emailController.text ,
        'name': userName,
        'gender': gender,
        'city': cityController.text,
        'address': addressController.text,
      };

      var header =
        {
          'Accept': 'application/json',
          'Authorization': 'Bearer $token'
        };
      print(body);
      print(header);
      final response = await http.post(
          baseUrl + 'onboard/setup-account-with-email',
          headers:header, body: body);
      if (response.statusCode == 200) {
        controller.reverse();
        Navigator.pushReplacementNamed(context, "/Login");
        responseData = json.decode(response.body);
        message = responseData["message"];
        data = responseData["data"];
        print(responseData);
        print(body);
        print(message);
        Fluttertoast.showToast(msg: message.toString());
      }
else
      {
        controller.reverse();
        responseData = json.decode(response.body);
        var er = responseData['message'];
        print('error$er');
        _showErrorDialog(message: er);
        Fluttertoast.showToast(msg: er, toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 3,
          backgroundColor: meatUpTheme,);
      }
    }
    else {
      controller.reverse();
      Fluttertoast.showToast(
          msg: 'Location cannot be empty', toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 3,
          backgroundColor: meatUpTheme);
    }
  }


    // ignore: must_call_super
    void initState() {
      // TODO: implement initState
      passwordVisible = true;
      _location();
      getToken();
    }


  @override
  Widget build(BuildContext context) {
    print('from almost there $token');
    print(token.runtimeType);
    final deviceWidth = MediaQuery
        .of(context)
        .size
        .width;

    final deviceHeight = MediaQuery
        .of(context)
        .size
        .height;

      if (emailController.text.isNotEmpty &&
          passwordController.text.isNotEmpty) {
        btnColor = meatUpTheme;
      }
      else {
        btnColor = Color(0xFFD4D4D4);
      }
    return
      Scaffold(
backgroundColor: backgroundColor,
        key: _scaffoldKey,

        body:
        ListView(
          children: <Widget>[
      Column(
            children: <Widget>[
              Stack(
                alignment: Alignment.bottomLeft,
                children: <Widget>[
                  Container(
                    child:
                    Image.asset(
                      'images/signup.png',
                      width: deviceWidth,
                      height: deviceHeight/4,
                      fit: BoxFit.fill,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(20),
                    height: 120,
                    width: 230,
                    child:
                    Image.asset(
                      'images/foodlogo.png',
                      fit: BoxFit.fill,
                    ),
                  ),
                ],
              ),

            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

            Container(
              padding: EdgeInsets.all(20),
              child:
              Text(
                'You are almost there...', style: TextStyle(color: Colors.black,
                  fontSize: 20, fontWeight: FontWeight.w600),
              ),
            ),
            Container(
              padding: EdgeInsets.all(20),
              child:
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                RichText(
                  text: TextSpan(
                      text: 'Password',
                      style: TextStyle(
                          color: Colors.black45, fontWeight: FontWeight.bold, fontSize: 12 ),
                     ),

                ),
                TextFormField(
                  controller: passwordController,
                  decoration: InputDecoration(
                    suffixIcon: IconButton(
                      icon: Icon(
                        // Based on passwordVisible state choose the icon
                        passwordVisible
                            ? Icons.visibility
                            : Icons.visibility_off,
                        color:  Colors.black,
                      ),
                      onPressed: () {
                        // Update the state i.e. toogle the state of passwordVisible variable
                        setState(() {
                          passwordVisible = !passwordVisible;
                        });
                      },
                    ),
                    filled: true,
                    fillColor: Colors.white30,
                    hintText: '*******',
                    hintStyle:TextStyle(
                      color: Colors.black45,
                    ) ,
                    labelStyle: TextStyle(color: Colors.blue),
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0),
                      borderSide: new BorderSide(),
                    ),
                  ),
                  obscureText: passwordVisible,
                  keyboardType: TextInputType.visiblePassword,
                  style: TextStyle(color: Colors.black),
                  cursorColor: Colors.black,
                ),
              ],
            ),
            ),

            SizedBox(
              height: 5,
            ),
            Container(
              padding: EdgeInsets.all(20),
    child:
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                        text: 'Phone number',
                        style: TextStyle(
                            color: Colors.black45, fontWeight: FontWeight.bold, fontSize: 12),
                        ),
                  ),
                  TextFormField(
                    controller: emailController,
                    decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                      hintText: '+23480000000',
                      hintStyle:TextStyle(
                      color: Colors.black45,
                      ) ,
                      labelStyle: TextStyle(color: Colors.blue),
                      border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0),
                        borderSide: new BorderSide(),
                      ),
                    ),
                    keyboardType: TextInputType.phone,
                    style: TextStyle(color: Colors.black),
                    cursorColor: Colors.black,
                  ),
                ],
              ),
            ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Gender',
                        style: TextStyle(
                            color: Colors.black45,
                            fontWeight: FontWeight.bold,
                            fontSize: 12),
                      ),
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    height: 60.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7.0),
                        border: Border.all(color: Colors.grey)),
                    child:
                      Center(
                        child: DropdownButtonFormField<String>(

                          isExpanded: true,
                          value: gender,

                          onChanged: (String newValue) {
                            setState(() {
                              gender = newValue;
                            });
                          },
                          decoration: InputDecoration.collapsed(
                              hintText: ''  ),
                          items: <String>[
                            'Select Gender',
                            'Male',
                            'Female',
                            'Others',
                          ].map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                        ),
                      )
                  ),
                    ],
                  ),
                ),
            SizedBox(
              height: 5,
            ),

                Container(
                  padding: EdgeInsets.all(20),
                  child:
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Drop-Off Address',
                        style: TextStyle(
                            color: Colors.black45, fontWeight: FontWeight.bold, fontSize: 12),
                      ),
                      TextFormField(
                        controller: addressController,
                        maxLines: null,
                        decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                          hintText: 'Your preferred location for drop-off',
                          hintStyle:TextStyle(
                            color: Colors.black45,
                          ) ,
                          labelStyle: TextStyle(color: Colors.blue),
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        keyboardType: TextInputType.multiline,
                        style: TextStyle(color: Colors.black),
                        cursorColor: Colors.black,
                      ),
                    ],
                  ),
                ),

                SizedBox(
                  height: 5,
                ),
                Container(
                  padding: EdgeInsets.all(20),
                  child:
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'City',
                        style: TextStyle(
                            color: Colors.black45, fontWeight: FontWeight.bold, fontSize: 12),
                      ),
                      TextFormField(
                        controller: cityController,
                        decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                          hintText: 'Enter city for drop-off',
                          hintStyle:TextStyle(
                            color: Colors.black45,
                          ) ,
                          labelStyle: TextStyle(color: Colors.blue),
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        keyboardType: TextInputType.text,
                        style: TextStyle(color: Colors.black),
                        cursorColor: Colors.black,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
            Container(
              padding: EdgeInsets.all(20),
              child:
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                        text: 'State',
                        style: TextStyle(
                            color: Colors.black45, fontWeight: FontWeight.bold, fontSize: 12),
                        ),

                  ),
    Container(
        padding: EdgeInsets.only(left: 10),
    height: 60.0,
    decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(7.0),
    border: Border.all(color: Colors.grey)),
    child:
                        Center(
                          child:
    new DropdownButtonFormField(
      decoration: InputDecoration.collapsed(
          hintText: ''  ),
    isExpanded: true,
    items: locations.map((item) {
    return new DropdownMenuItem(
    child: new Text(item['name']),
    value: item['id'].toString(),
    );
    }).toList(),
    onChanged: (newVal) {
    setState(() {
      _locationId = newVal;
    });
    },
    value: _locationId,
    ),
    )
    ),
                    ],
              ),
            ),
                Container(
                  padding: EdgeInsets.all(20),
                  width: deviceWidth,
                  child: ProgressButton(
                    borderRadius: BorderRadius.all(Radius.circular(7.0)),
                    color: btnColor,
                    strokeWidth: 2,
                    child: Text(
                      "Create Account & Get Started",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 12.0,
                        color: Colors.black,
                      ),
                    ),
                    onPressed: (AnimationController controller) {
                      createAccount( context, 1, controller,);
                    },
                  ),
                ),
            SizedBox(
              height: 15,
            ),
          ],
        ),
            ],
          )
    ]
        ),
      );
  }
}