import 'dart:convert';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:farmcrowdyfoods/Engine/httpDetails.dart';
import 'package:farmcrowdyfoods/colorconst.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_indicator_button/progress_button.dart';

//Making User phone number general
String uEmail;
String apiPhoneNo;
// ignore: must_be_immutable
class VerifyPin extends StatefulWidget {
 final String email;


  VerifyPin({Key key, this.email}) : super(key: key);
  @override
  _VerifyPin createState() => _VerifyPin(email);
}

class _VerifyPin extends State<VerifyPin> {
  String email;
  _VerifyPin(this.email);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    print(uEmail);
    uEmail = email;
    return Scaffold(
      body: PinCodeVerificationScreen(
          "$email"), // a random number, please don't call xD
    );
  }
}

class PinCodeVerificationScreen extends StatefulWidget {
  final String uEmail;
  PinCodeVerificationScreen(this.uEmail);
  @override
  _PinCodeVerificationScreenState createState() =>
      _PinCodeVerificationScreenState();
}

class _PinCodeVerificationScreenState extends State<PinCodeVerificationScreen> {
  var onTapRecognizer;

  TextEditingController textEditingController = TextEditingController();

  bool hasError = false;
  String currentText = "";
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  Map<String, dynamic> responseData;
  String message;
  var data;
  var token;

  SharedPreferences prefs;

  //Http call for resending OTP
  resendOtp() async {

    //  prefs = await SharedPreferences.getInstance();

    var body = {
      "phone_number": uEmail,
      "request_type": 'resendOTP'
    };
    print(data);
    final response = await http.post(
        baseUrl+ 'otp/resend',
        headers: {'Accept': 'application/json', 'Content-Type' : 'application/x-www-form-urlencoded'},  body: body);
    if (response.statusCode == 200) {
      // Navigator.pushReplacementNamed(context, "/VerifyPin");
      responseData = json.decode(response.body);
      message = responseData["message"];
      data = responseData["data"];
      print(responseData);
      print(body);
      print(message);
      Fluttertoast.showToast(msg: message);
    }
    else {
      // var er = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      Fluttertoast.showToast(msg: er);
    }
  }

//Http for verification
  verify( BuildContext context, int type, AnimationController controller)  async {
    controller.forward();
    prefs = await SharedPreferences.getInstance();
    var body = {
      "otp_code": currentText,
      "phone": uEmail
    };
    print(data);
    final response = await http.post(
        baseUrl+ 'otp/verify',
        headers: {'Accept': 'application/json', 'Content-Type' : 'application/x-www-form-urlencoded'},  body: body);
    if (response.statusCode == 200) {
      controller.reverse();
      Navigator.pushReplacementNamed(context, "/AlmostThere");
      responseData = json.decode(response.body);
      message = responseData["message"];
      //apiPhoneNo = responseData["data"]['user']['phone_number'];
      token = responseData['data']['access_token'];
      await prefs.setString('verifytoken', token);
      print('from verification $token');
      print(responseData);
      print(body);
      print(data);
      print(message);
      Fluttertoast.showToast(msg: message);
    }
    else {
      controller.reverse();
      // var er = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      Fluttertoast.showToast(msg: er);
    }
  }

  @override
  void initState() {
    onTapRecognizer = TapGestureRecognizer()
      ..onTap = () {
        resendOtp();
      };

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery
        .of(context)
        .size
        .width;
    final deviceHeight = MediaQuery
        .of(context)
        .size
        .height;
    print(uEmail);
    return Scaffold(
      key: scaffoldKey,
backgroundColor: backgroundColor,
      body:
        ListView(
        children: <Widget>[
          Stack(
            alignment: Alignment.bottomLeft,
            children: <Widget>[
              Container(
                child:
                Image.asset(
                  'images/signin.png',
                  width: deviceWidth,
                  height: deviceHeight/4,
                  fit: BoxFit.fill,
                ),
              ),
              Container(
                padding: EdgeInsets.all(20),
                height: 120,
                width: 230,
                child:
                Image.asset(
                  'images/foodlogo.png',
                  fit: BoxFit.fill,
                ),
              ),
            ],
          ),
      GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },

          child: Column(
            children: <Widget>[
//              Container(
//                height: MediaQuery.of(context).size.height /15,
//                child: FlareActor(
//                  "images/otp.flr",
//                  animation: "otp",
//                  fit: BoxFit.fitHeight,
//                  alignment: Alignment.center,
//                ),
//              ),
//               Image.asset(
//                 'images/verify.png',
//                 height: MediaQuery.of(context).size.height / 15,
//                 fit: BoxFit.fitHeight,
//               ),
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  'Verification Code',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  textAlign: TextAlign.center,
                ),
              ),

        Container(
        padding: EdgeInsets.all(30),
          child:
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Please enter the One-Time Pin (OTP)", style:
                    TextStyle(
                      fontSize: 16
                    ),
                  ),
                  Text(
                    "code sent to your phone ", style:
                  TextStyle(
                      fontSize: 16
                  ),
                  ),
                  SizedBox(
                    height: 20,
                  ),

                  InkWell(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child:
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
mainAxisAlignment: MainAxisAlignment.center,
children: <Widget>[

  Container(
    height: 18,
    width: 18,
    child:
  Image.asset('images/pencil.png'),
  ),
  SizedBox(
    width: 10,
  ),
  Text(
    widget.uEmail, style:
  TextStyle(
      fontSize: 16, color: meatUpTheme2
  ),
  ),

],
                  )
                  ),

                ],
              ),
        ),

//              Padding(
//                padding:
//                const EdgeInsets.symmetric(horizontal: 30.0, vertical: 8),
//                child: RichText(
//                  text: TextSpan(
//                      text: "Please enter the One-Time Pin (OTP) code sent to your phone ",
//                      children: [
//                        TextSpan(
//                            text: widget.uPhoneNo,
//                            style: TextStyle(
//                                color: Colors.black,
//                                fontWeight: FontWeight.bold,
//                                fontSize: 16)),
//                      ],
//                      style: TextStyle(color: Colors.black54, fontSize: 15)),
//                  textAlign: TextAlign.center,
//                ),
//              ),

              Container(
                width: deviceWidth,
                  padding: EdgeInsets.all(30),
                  //const EdgeInsets.symmetric(vertical: 1.0, horizontal: 90),
                  child: PinCodeTextField(
                    length: 5,
                    obsecureText: false,
                    inactiveColor: Color(0XFFC4C4C4) ,
                    activeColor: meatUpTheme2,
                    selectedColor: meatUpTheme2,
                    animationType: AnimationType.fade,
                    shape: PinCodeFieldShape.box,
                    animationDuration: Duration(milliseconds: 300),
                    borderRadius: BorderRadius.circular(5),
                    fieldHeight: 56,
                    fieldWidth: 50,
                    backgroundColor: backgroundColor,
                    controller: textEditingController,
                    onChanged: (value) {
                      print(value);
                      setState(() {
                        currentText = value;
                      });
                    },
                    onCompleted: (v) {
                      print('Finish$v');
                     // verify();
                    },
                  )),
              SizedBox(
                height: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
Container(
  padding: EdgeInsets.all(20),
  child:
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                    text: "Didn’t get the code? ",
                    style: TextStyle(color: Colors.black54, fontSize: 12),
                    children: [
                      TextSpan(
                          text: " Resend code.",
                          recognizer: onTapRecognizer,
                          style: TextStyle(
                              color: meatUpTheme2,
                              fontWeight: FontWeight.bold,
                              fontSize: 12))
                    ]),
              ),
),
              SizedBox(
                height: 14,
              ), Container(
                    padding: EdgeInsets.all(20),
                    width: deviceWidth,
                    child: ProgressButton(
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      color: meatUpTheme,
                      strokeWidth: 2,
                      child: Text(
                        "Verify Email",
                        style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: 12.0,
                          color: Colors.black,
                        ),
                      ),
                      onPressed: (AnimationController controller) {
                        verify(context, 1, controller,);
                      },
                    ),
                  ),
                ],
              )
            ],
          ),

      ),
    ]
      ),

    );
  }
}